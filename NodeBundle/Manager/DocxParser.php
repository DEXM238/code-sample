<?php

namespace Core\NodeBundle\Manager;


class DocxParser {

    const TYPE_TV = 1;
    const TYPE_RADIO = 2;
    const TYPE_PRESS = 3;
    const TYPE_NA = 4;

    private $imageMapping = null;
    private $filename;
    private $host;

    public function __construct($filename, $host){
        $this->filename = $filename;
        $this->host = $host;
    }

    public function extractImages(){
        $this->imageMapping = array();

        $pathinfo = pathinfo($this->filename);
        $dir = 'import_files/'.$this->translitIt($pathinfo['filename']);

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $xDocument = new \ZipArchive();
        if ($xDocument->open($this->filename) === true){
            $relsXml = $xDocument->getFromName('word/_rels/document.xml.rels');

            $reader = new \XMLReader();
            $reader->XML($relsXml);
            while($reader->read()){
                if(
                    ($reader->nodeType == \XMLReader::ELEMENT)
                    && ($reader->localName == 'Relationship')
                    && ($reader->getAttribute('Type') == 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/image')
                ){

                    $imageFileContent = $xDocument->getFromName('word/'.$reader->getAttribute('Target'));
                    $imageFilePaththinfo = pathinfo($reader->getAttribute('Target'));
                    $imageFileName = $dir.'/'.uniqid('image_').'.'.$imageFilePaththinfo['extension'];

                    file_put_contents($imageFileName, $imageFileContent);

                    $this->imageMapping[$reader->getAttribute('Id')] = $imageFileName;
                }
            }

        }
    }

    public function extractArticles($type){

        $this->extractImages();

        $articles = array();

        $xDocument = new \ZipArchive();
        if ($xDocument->open($this->filename) === true){

            $documentXml = $xDocument->getFromName('word/document.xml');

            $paragraphs = $this->extractParagraphs($documentXml);

           // $documentXml = implode("\n", $paragraphs);
            $documentXml = $paragraphs;

            $text =  strip_tags($documentXml, '<p>, <a:blip>, <v:shape>, <v:imagedata>');

            if($type == self::TYPE_NA){
                $delimiter = 'story';
            } else {
                $delimiter = 'source';
            }

            $rawArticles = explode('#'.$delimiter.'#', $text);

            foreach($rawArticles as $rawArticle){
                if(strpos($rawArticle, '#body#') == false)
                    continue;

                switch($type){
                    case self::TYPE_NA:
                        $article['story'] = strip_tags($this->getFirstValueBySynteticTag('story', $rawArticle));
                        $article['date'] = strip_tags($this->getValueBySyntheticTag('date', $rawArticle));
                        $article['source'] = strip_tags($this->getValueBySyntheticTag('source', $rawArticle));
                        $article['body'] = $this->replaceImages($this->getValueBySyntheticTag('body', $rawArticle));
                        break;
                    case self::TYPE_TV:
                        $article['date'] = strip_tags($this->getValueBySyntheticTag('date', $rawArticle));
                        $article['file'] = strip_tags($this->getValueBySyntheticTag('file', $rawArticle));
                        $article['start'] = $this->time2seconds(strip_tags($this->getValueBySyntheticTag('start', $rawArticle)));
                        $article['end'] = $this->time2seconds(strip_tags($this->getValueBySyntheticTag('end', $rawArticle)));
                        //если дата не заполнена, то парсим её из названия файла
                        if(empty($article['date']) && (!empty($article['file']))){
                            $fileParams = array_reverse(explode('_', preg_replace('/\.(mp4|mkv|flv|webm|mpg)/ius', '', $article['file'])));
                            if(isset($fileParams[0]) && isset($fileParams[1])){
                                $date = \DateTime::createFromFormat('d-m-Y H-i', $fileParams[0].' '.$fileParams[1]);
                                if($date){
                                    $article['date'] = $date->format('d.m.Y H:i');
                                }
                            }
                        }
                    case self::TYPE_RADIO:
                        $article['source'] = strip_tags($this->getSource($rawArticle));
                        $article['program'] = strip_tags($this->getValueBySyntheticTag('program', $rawArticle));
                        $article['story'] = strip_tags($this->getValueBySyntheticTag('story', $rawArticle));
                        $article['body'] = $this->replaceImages($this->getValueBySyntheticTag('body', $rawArticle));
                        $article['video'] = $this->getValueBySyntheticTagMultiple('video', $rawArticle);
                        $article['repeat'] = $this->getValueBySyntheticTagMultiple('repeat', $rawArticle);
                        $article['url'] = strip_tags($this->getValueBySyntheticTag('url', $rawArticle));

                        $article['body'] = '<p>' . implode(', ', $article['repeat']) . '</p><p>' . implode(', ', $article['video']) . '</p>' . $article['body'];
                        break;
                    case self::TYPE_PRESS:
                        $article['source'] = strip_tags($this->getSource($rawArticle));
                        $article['title'] = strip_tags($this->getValueBySyntheticTag('title', $rawArticle));
                        $article['part'] = strip_tags($this->getValueBySyntheticTag('part', $rawArticle));
                        $article['date'] = strip_tags($this->getValueBySyntheticTag('date', $rawArticle));
                        $article['author'] = strip_tags($this->getValueBySyntheticTag('author', $rawArticle));
                        $article['numpage'] = strip_tags($this->getValueBySyntheticTag('numpage', $rawArticle));
                        $article['body'] = $this->replaceImages($this->getValueBySyntheticTag('body', $rawArticle));
                        break;
                    default:
                        throw new \InvalidArgumentException('Неизвестный тип материала');
                }

                $articles[] = $article;
            }
        }

        return $articles;
    }

    private function extractParagraphs($xml){

        $reader = new \XMLReader();
        $reader->XML($xml);
        $paragraphs = '';

        while($reader->read()){
            if($reader->nodeType == \XMLReader::ELEMENT){
                if($reader->localName == 'p'){
                    $text = $reader->readInnerXml();
                    $text = strip_tags($text, '<a:blip>, <v:shape>, <v:imagedata>');
                    if(!empty($text)){
                        $paragraphs .= '<p>'.$text.'</p>';
                    }
                }
            }
        }
        return $paragraphs;
    }

    /**
     * @param $subject
     * @return mixed
     */
    private function replaceImages($subject){
        $withoutBlip = preg_replace_callback('/<a:blip r:embed="(\w*?)"(.*?)(\>\<\/a:blip>|\/\>)/u', function($match){

            $imageUrl = $this->host.'/'.$this->imageMapping[$match[1]];

            $size = getimagesize($imageUrl);

            return '<p><img src="'.$imageUrl.'" '.$size[3].' /></p>';

        }, $subject);

        $result = preg_replace_callback('/\<v:shape .*?style="width:([0-9\.]{1,})(pt|in);height:([0-9\.]{1,})(pt|in);.*?\<v:imagedata r:id="(.*?)".*?\<\/v:shape\>/sui', function($match){

            $imageUrl = $this->host.'/'.$this->imageMapping[$match[5]];

            $width = floor($match[1]);
            if($match[2] == 'in'){
                $width *= 72;
            }

            $height = floor($match[3]);
            if($match[4] == 'in'){
                $height*= 72;
            }

            return '<p><img src="'.$imageUrl.'" width="'.$width.'" height="'.$height.'" /></p>';

        }, $withoutBlip);

        return $result;
    }

    /**
     * @param $tag
     * @param $subject
     * @return bool|string
     */
    private function getValueBySyntheticTag($tag, $subject){
        $isMatch = preg_match('/\#'.$tag.'\#(.*?)(\#(\w*?)\#|$)/is', $subject, $matches);
        if($isMatch && ($matches[1] != '')){
            return trim(preg_replace('/\s+/u' , ' ', $matches[1]));
        } else {
            return null;
        }
    }

    /**
     * @param $tag
     * @param $subject
     * @return array
     */
    private function getValueBySyntheticTagMultiple($tag, $subject){
        $result = array();
        do{
            $isMatch = preg_match('/\#'.$tag.'\#(.*?)(\#(\w*?)\#|$)/is', $subject, $matches);
            if(!empty($matches)){
                $subject = str_replace(preg_replace('/\#'.$tag.'\#$/u', '', $matches[0]), '', $subject);
                $result[] = strip_tags(trim(preg_replace('/\s+/u' , ' ', $matches[1])));
            }

        }while($isMatch);

        return $result;
    }

    /**
     * @param $subject
     * @return bool|string
     */
    private function getSource($subject){
        $isMatch = preg_match('/(^|\#source\#)(.*?)(\#(\w*?)\#|$)/is', $subject, $matches);
        if($isMatch){
            return trim(preg_replace('/\s+/u' , ' ', $matches[2]));
        } else {
            return null;
        }
    }

    private function getFirstValueBySynteticTag($tag, $subject){
        $isMatch = preg_match('/(^|\#'.$tag.'\#)(.*?)(\#(\w*?)\#|$)/is', $subject, $matches);
        if($isMatch){
            return trim(preg_replace('/\s+/u' , ' ', $matches[2]));
        } else {
            return null;
        }
    }

    private function translitIt($str)
    {
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " "=> "_", "."=> "", "/"=> "_"
        );

        $urlstr = strtr($str,$tr);

        $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);

        return $urlstr;
    }

    private function time2seconds($time){
        $parsed = explode (":", $time);
        if(count($parsed) != 3){
            return 0;
        }
        list($h, $m, $s) = $parsed;
        $seconds = 0;
        $seconds += (intval($h) * 3600);
        $seconds += (intval($m) * 60);
        $seconds += (intval($s));
        return $seconds;
    }

} 