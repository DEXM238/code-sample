<?php

namespace Core\NodeBundle\Entity;
use Core\SourceBundle\Entity\Source;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Node
 * @ORM\Table(name="anp_node", indexes={
 *                              @ORM\Index(name="source_parser_id_index", columns={"source_id_parser"}),
 *                              @ORM\Index(name="original_index", columns={"original"}),
 *                              @ORM\Index(name="updated_index", columns={"updated"}),
 *                              @ORM\Index(name="deleted_index", columns={"deletedAt"})}
 *                              )
 * @ORM\Entity(repositoryClass="Core\NodeBundle\Entity\NodeRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Node
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\FileLogo")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="logo", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $logo;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\SourceType")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="source", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $source;

    /**
     * @ORM\ManyToMany(targetEntity="Core\SourceBundle\Entity\Source")
     * @ORM\JoinTable(name="anp_node_to_source")
     */
    private $nodeSource;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="releases", type="integer", nullable=true)
     */
    private $releases;

    /**
     * @var integer
     *
     * @ORM\Column(name="original", type="datetime", nullable=true)
     */
    private $original;

    /**
     * @var boolean
     *
     * @ORM\Column(name="daynews", type="boolean", nullable=true)
     */
    private $daynews;

    /**
     * @ORM\OneToOne(targetEntity="Core\GuideBundle\Entity\Reprint", mappedBy="id")
     */
    private $clone;
    /**
     * @ORM\ManyToMany(targetEntity="Core\GuideBundle\Entity\Simular")
     * @ORM\JoinTable(name="anp_node_simular")
     */

    private $simular;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\Author")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="author", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $author;

    /**
     * @var integer
     * @ORM\ManyToMany(targetEntity="Core\GuideBundle\Entity\Heading")
     * @ORM\JoinTable(name="anp_nodes_heading")
     */
    private $heading;

    /**
     * @var integer
     * @ORM\ManyToMany(targetEntity="Core\GuideBundle\Entity\Subheading")
     * @ORM\JoinTable(name="anp_nodes_heading_subheading")
     */
    private $subheading;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\Tonal")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="tonal", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $tonal;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\Status")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="status", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */


    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $body;

    /**
     * @ORM\ManyToMany(targetEntity="Core\ProjectBundle\Entity\Project", inversedBy="node")
     * @ORM\JoinTable(name="anp_node_project")
     */
    private $project;

    /**
     * @var integer
     *
     * @ORM\ManyToMany(targetEntity="Core\GuideBundle\Entity\Claster")
     * @ORM\JoinTable(name="anp_node_to_claster")
     *
     */
    private $claster;
    /**
     * @var integer
     *
     * @ORM\Column(name="press_series", type="string", nullable=true)
     */
    private $pressSeries;

    /**
     * @var integer
     *
     * @ORM\Column(name="press_page", type="string", nullable=true)
     */
    private $pressPage;

    /**
     * @var integer
     *
     * @ORM\Column(name="press_source", type="string", nullable=true)
     */
    private $pressSource;


    /**
     *  @Assert\File(maxSize="600M")
     */
    private $pressFile;

    /**
     * @ORM\Column(name="press", type="string", length=255, nullable=true)
     */
    private $press;
    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\TvProgram")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="tv_program", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $tvTitle;
    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\TvChannel")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="tv_channel", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $tvChannel;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\TvLeader")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="tv_leader", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $tvLeader;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\TvReporter")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="tv_reporter", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $tvReporter;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\RadioLeader")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="radio_leader", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $radioLeader;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\RadioProgarms")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="radio_programs", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $radioPrograms;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\RadioStation")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="radio_station", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $radioStation;

    /**
     * @ORM\Column(name="audio", type="string", length=255, nullable=true)
     */
    private $audio;

    /**
     * @Assert\File(maxSize="6000M")
     */
    private $audioFile;

    /**
     * @Assert\File(maxSize="6000M")
     */
    private $videoFile;

    /**
     * @Assert\File(maxSize="6000M")
     */
    private $stillsFile;

    /**
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @var integer
     *
     * @ORM\Column(name="video_seek", type="integer", nullable=true)
     */
    private $videoSeek = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="video_end", type="integer", nullable=true)
     */
    private $videoEnd = 0;

    /**
     * @var integer
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\GuideBundle\Entity\VideoStatus")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="video_status", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $videoStatus;

    /**
     * @ORM\Column(name="stills", type="string", length=255, nullable=true)
     */
    private $stills;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archive", type="boolean", nullable=true)
     */
    private $archive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;


    /**
     *
     * @ORM\ManyToOne(cascade={"persist"},targetEntity="Core\UserBundle\Entity\User")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="user", referencedColumnName="id", nullable=true, onDelete="SET NULL")})
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="source_id_parser", type="integer", nullable=true)
     */
    private $sourceParser;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="Core\SpiderBundle\Entity\Sitemap")
     * @ORM\JoinColumn(name="parser_sitemap_id", referencedColumnName="id")
     *
     */
    private $parserSitemap;

    /**
     * @ORM\ManyToMany(targetEntity="Core\GuideBundle\Entity\Keyword", inversedBy="node")
     * @ORM\JoinTable(name="anp_node_to_keyword")
     */
    private $keywords;

    /**
     * @ORM\ManyToMany(targetEntity="Core\WordpressPostingBundle\Entity\Blog", mappedBy="nodes")
     */
    private $blog;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="raw_original", type="string", length=255, nullable=true)
     */
    private $rawOriginal;

    /**
     * @ORM\OneToMany(targetEntity="Core\ReportBundle\Entity\ReportNode", mappedBy="node", cascade={"persist"})
     */
    private $reportNodes;

    /**
     * @var datetime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity="Core\NodeBundle\Entity\UploadVideo", mappedBy="nodes")
     */
    private $uploadVideos;

    public function __construct()
    {
        $this->setTitle('');
        $this->heading =  new ArrayCollection();
        $this->subheading =  new ArrayCollection();
        $this->claster =  new ArrayCollection();
        $this->keywords =  new ArrayCollection();
        $this->project =  new ArrayCollection();
        $this->nodeSource =  new ArrayCollection();
//        $this->setCreated(new \DateTime());
        $this->setOriginal(new \DateTime());
    }



    /**
     * Set audio
     *
     * @param string $audio
     * @return Node
     */
    public function setAudio($audio = NULL)
    {
        $this->audio = $audio;
        return $this;
    }

    /**
     * Set press
     *
     * @param string $press
     * @return Node
     */
    public function setPress($press = NULL)
    {
        $this->press = $press;
        return $this;
    }

    /**
     * Set video
     *
     * @param $video
     * @return Node
     */
    public function setVideo($video)
    {
        $this->video = $video;
        return $this;
    }

    /**
     * Get video
     *
     * @return integer 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Get audio
     *
     * @return integer
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * Get press
     *
     * @return integer
     */
    public function getPress()
    {
        return $this->press;
    }



    /**
     * Set user
     *
     * @param integer $user
     * @return Node
     */
    public function setUser(\Core\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Node
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Node
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set releases
     *
     * @param integer $releases
     * @return Node
     */
    public function setReleases($releases)
    {
        $this->releases = $releases;
    
        return $this;
    }

    /**
     * Get releases
     *
     * @return integer 
     */
    public function getReleases()
    {
        return $this->releases;
    }

    /**
     * Set original
     *
     * @param \DateTime $original
     * @return Node
     */
    public function setOriginal($original)
    {
        $this->original = $original;
        return $this;
    }

    /**
     * Get original
     *
     * @return \DateTime 
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * Set daynews
     *
     * @param boolean $daynews
     * @return Node
     */
    public function setDaynews($daynews)
    {
        $this->daynews = $daynews;
    
        return $this;
    }

    /**
     * Get daynews
     *
     * @return boolean 
     */
    public function getDaynews()
    {
        return $this->daynews;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Node
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set pressSeries
     *
     * @param string $pressSeries
     * @return Node
     */
    public function setPressSeries($pressSeries)
    {
        $this->pressSeries = $pressSeries;
    
        return $this;
    }

    /**
     * Get pressSeries
     *
     * @return string 
     */
    public function getPressSeries()
    {
        return $this->pressSeries;
    }

    /**
     * Set pressPage
     *
     * @param string $pressPage
     * @return Node
     */
    public function setPressPage($pressPage)
    {
        $this->pressPage = $pressPage;
    
        return $this;
    }

    /**
     * Get pressPage
     *
     * @return string 
     */
    public function getPressPage()
    {
        return $this->pressPage;
    }

    /**
     * Set pressSource
     *
     * @param string $pressSource
     * @return Node
     */
    public function setPressSource($pressSource)
    {
        $this->pressSource = $pressSource;
    
        return $this;
    }

    /**
     * Get pressSource
     *
     * @return string 
     */
    public function getPressSource()
    {
        return $this->pressSource;
    }

    /**
     * Set pressFile
     *
     * @param UploadedFile $pressFile
     * @return Node
     */
    public function setPressFile(UploadedFile $pressFile = null)
    {
        $this->pressFile = $pressFile;
        return $this;
    }

    /**
     * Get pressFile
     *
     * @return string 
     */
    public function getPressFile()
    {
        return $this->pressFile;
    }

    /**
     * Set stills
     *
     * @param $stills
     * @return Node
     */
    public function setStills($stills=null)
    {
        $this->stills = $stills;
        return $this;
    }

    /**
     * Get stills
     *
     * @return string 
     */
    public function getStills()
    {
        return $this->stills;
    }

    /**
     * Set archive
     *
     * @param boolean $archive
     * @return Node
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    
        return $this;
    }

    /**
     * Get archive
     *
     * @return boolean 
     */
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Node
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set logo
     *
     * @param \Core\GuideBundle\Entity\FileLogo $logo
     * @return Node
     */
    public function setLogo(\Core\GuideBundle\Entity\FileLogo $logo = null)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return \Core\GuideBundle\Entity\FileLogo 
     */
    public function getLogo()
    {
        return $this->logo;
    }


    /**
     * Add nodeSource
     *
     * @param \Core\SourceBundle\Entity\Source $nodeSource
     * @return Node
     */
    public function addNodeSource(\Core\SourceBundle\Entity\Source $nodeSource)
    {
        $this->nodeSource[] = $nodeSource;
    
        return $this;
    }

    /**
     * Remove nodeSource
     *
     * @param \Core\SourceBundle\Entity\Source $nodeSource
     */
    public function removeNodeSource(\Core\SourceBundle\Entity\Source $nodeSource)
    {
        $this->nodeSource->removeElement($nodeSource);
    }

    /**
     * Get nodeSource
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNodeSource()
    {
        return $this->nodeSource;
    }

    /**
     * Add simular
     *
     * @param \Core\GuideBundle\Entity\Simular $simular
     * @return Node
     */
    public function addSimular(\Core\GuideBundle\Entity\Simular $simular)
    {
        $this->simular[] = $simular;
    
        return $this;
    }

    /**
     * Remove simular
     *
     * @param \Core\GuideBundle\Entity\Simular $simular
     */
    public function removeSimular(\Core\GuideBundle\Entity\Simular $simular)
    {
        $this->simular->removeElement($simular);
    }

    /**
     * Get simular
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSimular()
    {
        return $this->simular;
    }

    /**
     * Set author
     *
     * @param \Core\GuideBundle\Entity\Author $author
     * @return Node
     */
    public function setAuthor(\Core\GuideBundle\Entity\Author $author = null)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return \Core\GuideBundle\Entity\Author 
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * Set tonal
     *
     * @param \Core\GuideBundle\Entity\Tonal $tonal
     * @return Node
     */
    public function setTonal(\Core\GuideBundle\Entity\Tonal $tonal = null)
    {
        $this->tonal = $tonal;
    
        return $this;
    }

    /**
     * Get tonal
     *
     * @return \Core\GuideBundle\Entity\Tonal 
     */
    public function getTonal()
    {
        return $this->tonal;
    }

    /**
     * Set status
     *
     * @param \Core\GuideBundle\Entity\Status $status
     * @return Node
     */
    public function setStatus(\Core\GuideBundle\Entity\Status $status = null)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return \Core\GuideBundle\Entity\Status 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add project
     *
     * @param \Core\ProjectBundle\Entity\Project $project
     * @return Node
     */
    public function addProject(\Core\ProjectBundle\Entity\Project $project)
    {
        $this->project[] = $project;
    
        return $this;
    }

    /**
     * Remove project
     *
     * @param \Core\ProjectBundle\Entity\Project $project
     */
    public function removeProject(\Core\ProjectBundle\Entity\Project $project)
    {
        $this->project->removeElement($project);
    }

    /**
     * Get project
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProject()
    {
        return $this->project;
    }


    /**
     * Set tvTitle
     *
     * @param \Core\GuideBundle\Entity\TvProgram $tvTitle
     * @return Node
     */
    public function setTvTitle(\Core\GuideBundle\Entity\TvProgram $tvTitle = null)
    {
        $this->tvTitle = $tvTitle;
    
        return $this;
    }

    /**
     * Get tvTitle
     *
     * @return \Core\GuideBundle\Entity\TvProgram 
     */
    public function getTvTitle()
    {
        return $this->tvTitle;
    }

    /**
     * Set tvChannel
     *
     * @param \Core\GuideBundle\Entity\TvChannel $tvChannel
     * @return Node
     */
    public function setTvChannel(\Core\GuideBundle\Entity\TvChannel $tvChannel = null)
    {
        $this->tvChannel = $tvChannel;
    
        return $this;
    }

    /**
     * Get tvChannel
     *
     * @return \Core\GuideBundle\Entity\TvChannel 
     */
    public function getTvChannel()
    {
        return $this->tvChannel;
    }

    /**
     * Set tvLeader
     *
     * @param \Core\GuideBundle\Entity\TvLeader $tvLeader
     * @return Node
     */
    public function setTvLeader(\Core\GuideBundle\Entity\TvLeader $tvLeader = null)
    {
        $this->tvLeader = $tvLeader;
    
        return $this;
    }

    /**
     * Get tvLeader
     *
     * @return \Core\GuideBundle\Entity\TvLeader 
     */
    public function getTvLeader()
    {
        return $this->tvLeader;
    }

    /**
     * Set tvReporter
     *
     * @param \Core\GuideBundle\Entity\TvReporter $tvReporter
     * @return Node
     */
    public function setTvReporter(\Core\GuideBundle\Entity\TvReporter $tvReporter = null)
    {
        $this->tvReporter = $tvReporter;
    
        return $this;
    }

    /**
     * Get tvReporter
     *
     * @return \Core\GuideBundle\Entity\TvReporter 
     */
    public function getTvReporter()
    {
        return $this->tvReporter;
    }

    /**
     * Set videoStatus
     *
     * @param \Core\GuideBundle\Entity\VideoStatus $videoStatus
     * @return Node
     */
    public function setVideoStatus(\Core\GuideBundle\Entity\VideoStatus $videoStatus = null)
    {
        $this->videoStatus = $videoStatus;
    
        return $this;
    }

    /**
     * Get videoStatus
     *
     * @return \Core\GuideBundle\Entity\VideoStatus 
     */
    public function getVideoStatus()
    {
        return $this->videoStatus;
    }

    /**
     * Set radioLeader
     *
     * @param \Core\GuideBundle\Entity\RadioLeader $radioLeader
     * @return Node
     */
    public function setRadioLeader(\Core\GuideBundle\Entity\RadioLeader $radioLeader = null)
    {
        $this->radioLeader = $radioLeader;
    
        return $this;
    }

    /**
     * Get radioLeader
     *
     * @return \Core\GuideBundle\Entity\RadioLeader 
     */
    public function getRadioLeader()
    {
        return $this->radioLeader;
    }

    /**
     * Set radioPrograms
     *
     * @param \Core\GuideBundle\Entity\RadioProgarms $radioPrograms
     * @return Node
     */
    public function setRadioPrograms(\Core\GuideBundle\Entity\RadioProgarms $radioPrograms = null)
    {
        $this->radioPrograms = $radioPrograms;
    
        return $this;
    }

    /**
     * Get radioPrograms
     *
     * @return \Core\GuideBundle\Entity\RadioProgarms 
     */
    public function getRadioPrograms()
    {
        return $this->radioPrograms;
    }

    /**
     * Set radioStation
     *
     * @param \Core\GuideBundle\Entity\RadioStation $radioStation
     * @return Node
     */
    public function setRadioStation(\Core\GuideBundle\Entity\RadioStation $radioStation = null)
    {
        $this->radioStation = $radioStation;
    
        return $this;
    }

    /**
     * Get radioStation
     *
     * @return \Core\GuideBundle\Entity\RadioStation 
     */
    public function getRadioStation()
    {
        return $this->radioStation;
    }

    /**
     * Add heading
     *
     * @param \Core\GuideBundle\Entity\Heading $heading
     * @return Node
     */
    public function addHeading(\Core\GuideBundle\Entity\Heading $heading)
    {
        $this->heading[] = $heading;
    
        return $this;
    }

    /**
     * Remove heading
     *
     * @param \Core\GuideBundle\Entity\Heading $heading
     */
    public function removeHeading(\Core\GuideBundle\Entity\Heading $heading)
    {
        $this->heading->removeElement($heading);
    }

    /**
     * Get heading
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHeading()
    {
        return $this->heading;
    }

    /**
     * Add subheading
     *
     * @param \Core\GuideBundle\Entity\Subheading $subheading
     * @return Node
     */
    public function addSubheading(\Core\GuideBundle\Entity\Subheading $subheading)
    {
        $this->subheading[] = $subheading;
    
        return $this;
    }

    /**
     * Remove subheading
     *
     * @param \Core\GuideBundle\Entity\Subheading $subheading
     */
    public function removeSubheading(\Core\GuideBundle\Entity\Subheading $subheading)
    {
        $this->subheading->removeElement($subheading);
    }

    /**
     * Get subheading
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubheading()
    {
        return $this->subheading;
    }

    /**
     * Set sourceParser
     *
     * @param integer $sourceParser
     * @return Node
     */
    public function setSourceParser($sourceParser)
    {
        $this->sourceParser = $sourceParser;
    
        return $this;
    }

    /**
     * Get sourceParser
     *
     * @return integer 
     */
    public function getSourceParser()
    {
        return $this->sourceParser;
    }

    /**
     * Add claster
     *
     * @param \Core\GuideBundle\Entity\Claster $claster
     * @return Node
     */
    public function addClaster(\Core\GuideBundle\Entity\Claster $claster)
    {
        $this->claster[] = $claster;
    
        return $this;
    }

    /**
     * Remove claster
     *
     * @param \Core\GuideBundle\Entity\Claster $claster
     */
    public function removeClaster(\Core\GuideBundle\Entity\Claster $claster)
    {
        $this->claster->removeElement($claster);
    }

    /**
     * Get claster
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClaster()
    {
        return $this->claster;
    }

    /**
     * Set parserSitemap
     *
     * @param \Core\SpiderBundle\Entity\Sitemap $parserSitemap
     * @return Node
     */
    public function setParserSitemap(\Core\SpiderBundle\Entity\Sitemap $parserSitemap = null)
    {
        $this->parserSitemap = $parserSitemap;

        return $this;
    }

    /**
     * Get parserSitemap
     *
     * @return \Core\SpiderBundle\Entity\Sitemap
     */
    public function getParserSitemap()
    {
        return $this->parserSitemap;
    }

    /**
     * Add keywords
     *
     * @param \Core\GuideBundle\Entity\Keyword $keywords
     * @return Node
     */
    public function addKeyword(\Core\GuideBundle\Entity\Keyword $keywords)
    {
        $this->keywords[] = $keywords;
    
        return $this;
    }

    /**
     * Remove keywords
     *
     * @param \Core\GuideBundle\Entity\Keyword $keywords
     */
    public function removeKeyword(\Core\GuideBundle\Entity\Keyword $keywords)
    {
        $this->keywords->removeElement($keywords);
    }

    /**
     * Get keywords
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Node
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }



    /**
     * Set source
     *
     * @param \Core\GuideBundle\Entity\SourceType $source
     * @return Node
     */
    public function setSource(\Core\GuideBundle\Entity\SourceType $source = null)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return \Core\GuideBundle\Entity\SourceType 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set clone
     *
     * @param \Core\GuideBundle\Entity\Reprint $clone
     * @return Node
     */
    public function setClone(\Core\GuideBundle\Entity\Reprint $clone = null)
    {
        $this->clone = $clone;
    
        return $this;
    }

    /**
     * Get clone
     *
     * @return \Core\GuideBundle\Entity\Reprint 
     */
    public function getClone()
    {
        return $this->clone;
    }

    /**
     * Set raw original
     *
     * @param string $rawOriginal
     * @return Node
     */
    public function setRawOriginal($rawOriginal)
    {
        $str = (strlen($rawOriginal) > 250) ? substr($rawOriginal, 0, 250) : $rawOriginal;
        $this->rawOriginal = $str;

        return $this;
    }

    /**
     * Get raw original
     *
     * @return string
     */
    public function getRawOriginal()
    {
        return $this->rawOriginal;
    }

    /**
     * Sets audio file.
     *
     * @param UploadedFile $file
     */
    public function setAudioFile(UploadedFile $file = null)
    {
        $this->audioFile = $file;
    }

    /**
     * Get audio file.
     *
     * @return UploadedFile
     */
    public function getAudioFile()
    {
        return $this->audioFile;
    }

    /**
     * Sets video file.
     *
     * @param UploadedFile $file
     */
    public function setVideoFile(UploadedFile $file = null)
    {
        $this->videoFile = $file;
    }

    /**
     * Get video file.
     *
     * @return UploadedFile
     */
    public function getVideoFile()
    {
        return $this->videoFile;
    }

    /**
     * Sets audio file.
     *
     * @param UploadedFile $file
     */
    public function setStillsFile(UploadedFile $file = null)
    {
        $this->stillsFile = $file;
    }

    /**
     * Get stills file.
     *
     * @return UploadedFile
     */
    public function getStillsFile()
    {
        return $this->stillsFile;
    }

    public function getWebPath($type)
    {
        return './tmp/'.$type;
    }

    protected function getUploadRootDir($type)
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir($type);
    }

    protected function getUploadDir($type)
    {
        return '/tmp/'.$type;
    }

    public function upload()
    {
        if (null !== $this->getAudioFile()) {
            $tmpname = uniqid().'.'.$this->audioFile->guessClientExtension();
            $this->audioFile->move($this->getUploadRootDir('audio'), $tmpname);
            $this->setAudio($this->getWebPath('audio').'/'.$tmpname);

            // очистка свойства, поскольку оно более не понадобится
            $this->audioFile = null;
        }

        if(null !== $this->getVideoFile()){
            $tmpname = uniqid().'.'.$this->videoFile->guessClientExtension();
            $this->videoFile->move($this->getUploadRootDir('video'), $tmpname);
            $this->setVideo($this->getWebPath('video').'/'.$tmpname);

            $this->videoFile = null;
        }

        if(null !== $this->getStillsFile()){
            $tmpname = uniqid().'.'.$this->stillsFile->guessClientExtension();
            $this->stillsFile->move($this->getUploadRootDir('stills'), $tmpname);
            $this->setStills($this->getWebPath('stills').'/'.$tmpname);

            $this->stillsFile = null;
        }

        if(null !== $this->getPressFile()){
            $tmpname = uniqid().'.'.$this->pressFile->guessClientExtension();
            $this->pressFile->move($this->getUploadRootDir('press'), $tmpname);
            $this->setPress($this->getWebPath('press').'/'.$tmpname);

            $this->pressFile = null;
        }
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Node
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add reportNodes
     *
     * @param \Core\ReportBundle\Entity\ReportNode $reportNodes
     * @return Node
     */
    public function addReportNode(\Core\ReportBundle\Entity\ReportNode $reportNodes)
    {
        $this->reportNodes[] = $reportNodes;
    
        return $this;
    }

    /**
     * Remove reportNodes
     *
     * @param \Core\ReportBundle\Entity\ReportNode $reportNodes
     */
    public function removeReportNode(\Core\ReportBundle\Entity\ReportNode $reportNodes)
    {
        $this->reportNodes->removeElement($reportNodes);
    }

    /**
     * Get reportNodes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportNodes()
    {
        return $this->reportNodes;
    }

    /**
     * Add blog
     *
     * @param \Core\WordpressPostingBundle\Entity\Blog $blog
     * @return Node
     */
    public function addBlog(\Core\WordpressPostingBundle\Entity\Blog $blog)
    {
        $this->blog[] = $blog;
    
        return $this;
    }

    /**
     * Remove blog
     *
     * @param \Core\WordpressPostingBundle\Entity\Blog $blog
     */
    public function removeBlog(\Core\WordpressPostingBundle\Entity\Blog $blog)
    {
        $this->blog->removeElement($blog);
    }

    /**
     * Get blog
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Set videoSeek
     *
     * @param integer $videoSeek
     * @return Node
     */
    public function setVideoSeek($videoSeek)
    {
        $this->videoSeek = $videoSeek;
    
        return $this;
    }

    /**
     * Get videoSeek
     *
     * @return integer 
     */
    public function getVideoSeek()
    {
        return $this->videoSeek;
    }

    /**
     * Add uploadVideos
     *
     * @param \Core\NodeBundle\Entity\UploadVideo $uploadVideos
     * @return Node
     */
    public function addUploadVideo(\Core\NodeBundle\Entity\UploadVideo $uploadVideos)
    {
        $this->uploadVideos[] = $uploadVideos;
        $uploadVideos->addNode($this);
    
        return $this;
    }

    /**
     * Remove uploadVideos
     *
     * @param \Core\NodeBundle\Entity\UploadVideo $uploadVideos
     */
    public function removeUploadVideo(\Core\NodeBundle\Entity\UploadVideo $uploadVideos)
    {
        $this->uploadVideos->removeElement($uploadVideos);
    }

    /**
     * Get uploadVideos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUploadVideos()
    {
        return $this->uploadVideos;
    }

    /**
     * Set videoEnd
     *
     * @param integer $videoEnd
     * @return Node
     */
    public function setVideoEnd($videoEnd)
    {
        $this->videoEnd = $videoEnd;
    
        return $this;
    }

    /**
     * Get videoEnd
     *
     * @return integer 
     */
    public function getVideoEnd()
    {
        return $this->videoEnd;
    }
}