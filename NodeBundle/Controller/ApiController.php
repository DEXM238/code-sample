<?php

namespace Core\NodeBundle\Controller;

use Core\NodeBundle\Entity\Node;
use Core\NodeBundle\Entity\UploadVideo;
use Core\SpiderBundle\Entity\ProxyServer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Node API controller.
 *
 * @Route("/api")
 */
class ApiController extends Controller{

 
    /**
     * Add new document
     *
     * @Route("/node", name="api_create_node")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $params = json_decode($request->getContent(), true);

        $requiredParams = ['title', 'body', 'original', 'sourceType', 'source', 'video'];
        $requestParams = array_keys($params);
        foreach($requiredParams as $requiredParam){
            if(!in_array($requiredParam, $requestParams)){
                throw new HttpException(400);
            }
        }

        $em = $this->getDoctrine()->getManager();

        $node = new Node();
        $node->setCreated(new \DateTime());
        $node->setTitle($params['title']);
        $node->setBody($params['body']);
        $node->setVideo($params['video']);
        $node->setSource($em->getRepository('CoreGuideBundle:SourceType')->findOneById($params['sourceType']));
        $node->setOriginal(\DateTime::createFromFormat('U', $params['original']));

        $source = $em->getRepository('CoreSourceBundle:Source')->findOneBy([
            'title' => $params['source'],
            'type' => $params['sourceType'],
        ]);
        if($source){
            $node->addNodeSource($source);
        } else {
            throw new HttpException(400, 'Невалидный источник');
        }

        //тв-материал
        if($params['sourceType'] == 1 && (isset($params['program']))){

            $tvProgram = $em->getRepository('CoreGuideBundle:TvProgram')->findOneBy([
                'name' => $params['program'],
                'source' => $source,
            ]);

            if(!is_null($params['program']) && (!$tvProgram)){
                throw new HttpException(400, 'Невалидная программа');
            }

            $node->setTvTitle($tvProgram);
        }

        $validator = $this->get('validator');
        $errors    = $validator->validate($node);

        if (count($errors) > 0) {
            throw new HttpException(400);
        } else {
            $em->persist($node);
            $em->flush($node);
            $em->clear();

            return new Response(json_encode(['id' => $node->getId()]));
        }
    }

    /**
     * @Route("/video/check_parsed_data", name="api_check_parsed_data")
     * @Method("GET")
     */
    public function checkSourceAndProgram(Request $request){
        $em = $this->getDoctrine()->getManager();

        $sourceParam = $request->query->get('source');
        $programParam = $request->query->get('program');

        $source = $em->getRepository('CoreSourceBundle:Source')->findOneBy([
            'title' => $sourceParam,
            'type' => 1,
        ]);
        if(!$source){
            throw new HttpException(400, 'Невалидный источник');
        }

        if($programParam){
            $program = $em->getRepository('CoreGuideBundle:TvProgram')->findOneBy([
                'name' => $programParam,
                'source' => $source,
            ]);
            if(!$program){
                throw new HttpException(400, 'Невалидный источник');
            }
        }

        return new Response();
    }

    /**
     * @Route("/node/{id}", name="api_update_node")
     * @Method("PUT")
     */
    public function updateNodeAction(Request $request, $id){
        $params = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        /** @var Node $entity */
        $entity = $em->getRepository('CoreNodeBundle:Node')->findOneById($id);

        if(!$entity){
            throw new HttpException(404);
        }

        if(isset($params['video'])){
            $entity->setVideo($params['video']);
        }
        $em->flush($entity);
        $em->clear();
        return new Response();
    }

    /**
     * @Route("/video", name="api_create_video")
     * @Method("POST")
     */
    public function createVideoAction(Request $request){
        $params = json_decode($request->getContent(), true);

        $requiredParams = ['filePath', 'fileHash'];
        $requestParams = array_keys($params);
        foreach($requiredParams as $requiredParam){
            if(!in_array($requiredParam, $requestParams)){
                throw new HttpException(400);
            }
        }


        $em = $this->getDoctrine()->getManager();

        $video = new UploadVideo();
        $video->setFilePath($params['filePath']);
        $video->setFileHash($params['fileHash']);

        if(isset($params['nodeId']) && (!is_null($params['nodeId']))){
            $video->addNode($em->getRepository('CoreNodeBundle:Node')->findOneById($params['nodeId']));
        }

        $validator = $this->get('validator');
        $errors    = $validator->validate($video);

        if (count($errors) > 0) {
            throw new HttpException(400);
        } else {
            $em->persist($video);
            $em->flush();
            $em->clear();

            return new Response($video->getId());
        }
    }

    /**
     * @Route("/video/is_exist", name="api_is_exist_video")
     * @Method("GET")
     */
    public function isExistVideoAction(Request $request){
        $fileHash = $request->query->get('fileHash');
        $filePath = $request->query->get('filePath');

        if((!$fileHash) && (!$filePath)){
            throw new HttpException(400);
        }

        if($fileHash){
            $entity = $this->getDoctrine()->getManager()->getRepository('CoreNodeBundle:UploadVideo')->findOneByFileHash($fileHash);
        } else{
            $entity = $this->getDoctrine()->getManager()->getRepository('CoreNodeBundle:UploadVideo')->findOneByFilePathHash(md5($filePath));
        }

        if(!$entity){
            throw new HttpException(404);
        }
        return new Response();
    }

    /**
     * @Route("/video", name="api_list_video")
     * @Method("GET")
     */
    public function listVideoAction(Request $request){
        $notConverted = (boolean)$request->query->get('not_converted');
        if(!$notConverted){
            throw new HttpException(400);
        }

        $entities = $this->getDoctrine()
            ->getManager()
            ->getRepository('CoreNodeBundle:UploadVideo')
            ->findByConvertedFilePath(null);

        $serializer = $this->container->get('serializer');
        $json = $serializer->serialize($entities, 'json');
        $headers = array( 'Content-type' => 'application-json; charset=utf8' );
        return new Response($json, 200, $headers);
    }

    /**
     * @Route("/video/{id}", name="api_update_video")
     * @Method("PUT")
     */
    public function updateVideoAction(Request $request, $id){
        $params = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        /** @var UploadVideo $entity */
        $entity = $em->getRepository('CoreNodeBundle:UploadVideo')->findOneById($id);

        if(!$entity){
            throw new HttpException(404);
        }

        if(isset($params['converted_file_path'])){
            $entity->setConvertedFilePath($params['converted_file_path']);
        }

        if(isset($params['nodeId']) && (!is_null($params['nodeId']))){
            $entity->addNode($em->getRepository('CoreNodeBundle:Node')->findOneById($params['nodeId']));
        }

        $em->flush($entity);
        $em->clear();
        return new Response();
    }

    private function validateParams(array $requiredParams, array $requestParams){
        foreach($requiredParams as $requiredParam){
            if(!in_array($requiredParam, $requestParams)){
                throw new HttpException(400);
            }
        }
    }
} 