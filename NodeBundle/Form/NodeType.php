<?php

namespace Core\NodeBundle\Form;

use Core\GuideBundle\Entity\Claster;
use Core\NodeBundle\Entity\Node;
use Core\NodeBundle\Form\DataTransformer\TimeToSecondsTransformer;
use Core\NodeBundle\Form\Type\ClastersType;
use Core\UserBundle\Form\NodeFieldSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;


class NodeType extends AbstractType
{
    protected $em;
    protected $entity;
    protected $container;

    public function __construct(ContainerInterface $container, Node $node = null)
    {
        $this->em = $container->get('doctrine')->getManager();
        $this->entity = $node;
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $secondsTransformer = new TimeToSecondsTransformer();

        $builder
            ->add('title')
            ->add('url')
            ->add('releases')
            ->add('daynews')
            ->add('body')
            ->add('pressSeries')
            ->add('nodeSource', 'entity', array(
                'class' => 'Core\SourceBundle\Entity\Source',
                'property' => 'title',
                'multiple' => true,
                'required' => false,
                'query_builder' => function(EntityRepository $er) use ($type){
                    return $er->createQueryBuilder('s')
                        ->where('s.type = ?1')
                        ->setParameter(1, $type);
                }
            ))
            ->add('pressPage')
            ->add('pressSource')
            ->add('archive')
            ->add('created')
            ->add('logo')
            ->add('source')
            ->add('simular')
            ->add('author')
            ->add('heading')
            ->add('subheading')
            ->add('status')
            ->add('project', null, array('required' => false))
            ->add('claster', 'entity', array(
                'class' => 'CoreGuideBundle:Claster',
                'multiple' => true,
                'required' => false,
            ))
            ->add('pressFile', 'file', array('data_class' => null,'required' => false))
            ->add('tvTitle')
            ->add('tvChannel')
            ->add('tvLeader')
            ->add('tvReporter')
            ->add('radioLeader')
            ->add('radioStation')
            ->add('radioPrograms')
            ->add('audioFile')
            ->add('videoFile')
            ->add('stillsFile')
            ->add('videoStatus')
            ->add('user')
            ->add(
                $builder->create('videoSeek', 'text', ['required' => false])
                    ->addModelTransformer($secondsTransformer)
            )
            ->add(
                $builder->create('videoEnd', 'text', ['required' => false])
                    ->addModelTransformer($secondsTransformer)
            )
        ;

        $subscriber = new NodeFieldSubscriber($builder->getFormFactory());
        $subscriber->injectUser($this->container->get('fos_user.user_manager'), $this->container->get('security.context'));
        $builder->addEventSubscriber($subscriber);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Core\NodeBundle\Entity\Node'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'core_nodebundle_node';
    }
}
