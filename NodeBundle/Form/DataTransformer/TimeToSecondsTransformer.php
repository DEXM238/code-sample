<?php

namespace Core\NodeBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class TimeToSecondsTransformer implements DataTransformerInterface {

    public function transform($seconds){
        return gmdate('H:i:s', $seconds);
    }
    public function reverseTransform($time){
        if(is_null($time)){
            return null;
        }
        list($h, $m, $s) = explode (":", $time);
        $seconds = 0;
        $seconds += (intval($h) * 3600);
        $seconds += (intval($m) * 60);
        $seconds += (intval($s));
        return $seconds;
    }

}