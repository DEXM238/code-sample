<?php

namespace Core\NodeBundle\Form\Type;

use Core\NodeBundle\Entity\Node;
use Core\ProjectBundle\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;

class ClastersType extends AbstractType
{
    protected $em;
    protected $data;

    public function __construct(EntityManager $em = null, $entity = null)
    {
        $this->em = $em;
        $this->data = $entity;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'clasters_type';
    }

    public function getParent()
    {
        return 'entity';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => 'Core\GuideBundle\Entity\Claster',
            'multiple' => true,
            'by_reference' => true,
            'query_builder' => function(EntityRepository $er){
                    $projectId = ($this->data instanceof Project) ? $this->data->getId() : 0;
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.project', 'project')
                        ->where('project.id = :project_id')
                        ->setParameter('project_id', $projectId);
                }
        ));

        $resolver->setOptional(array('node'));
    }

}