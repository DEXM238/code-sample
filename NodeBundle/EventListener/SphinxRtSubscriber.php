<?php

namespace Core\NodeBundle\EventListener;

use Core\NodeBundle\Entity\NodeSubheading;
use Core\SourceBundle\Entity\Source;
use Core\SphinxBundle\Client\SphinxRT;
use Core\SphinxBundle\QueryBuilder\QueryBuilder;
use Doctrine\Common\EventSubscriber;
use Core\NodeBundle\Entity\Node;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Tools\Export\ClassMetadataExporter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add documents to Sphinx Runtime Index
 *
 * Class SphinxRtSubscriber
 * @package Core\NodeBundle\EventListener
 */
class SphinxRtSubscriber implements EventSubscriber {

    private $container;

    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'preRemove',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Node && is_null($entity->getSourceParser())) {
            $sphinxRt = new SphinxRT($this->container);
            $result = $this->getData($entity);
            $sphinxRt->insert($result['data'], $result['types']);
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {

        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if($entity instanceof Node) {

            if (is_null($entity->getSourceParser())) {
                $sphinxRt = new SphinxRT($this->container);
                $sphinxRt->delete($entity->getId());
            }

            $sphinx =  new QueryBuilder($em, 'CoreNodeBundle:Node', $this->container, true);
            $sphinx->delete($entity);
        }
    }

} 