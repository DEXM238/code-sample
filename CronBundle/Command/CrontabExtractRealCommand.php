<?php
namespace Core\CronBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class CrontabExtractRealCommand extends ContainerAwareCommand {

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName( 'cron:extract' )
            ->setDescription('Extract real crontab');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute( InputInterface $input, OutputInterface $output )
    {
        $crontabFile = getcwd().'/web/cronbike';

        $process = new Process('crontab -l');
        $process->run();
        $crontabContent = $process->getOutput();

        $flag = file_put_contents($crontabFile, $crontabContent);
        if($flag !== false){
            $output->writeLn('<fg=green>Extracted</fg=green>');
        } else {
            $output->writeLn('<fg=red> Error: .$process->getErrorOutput().</fg=red>');
        }
    }
} 