<?php

namespace Core\CronBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class CrontabUpdateCommand extends ContainerAwareCommand {

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName( 'cron:update' )
            ->setDescription('Crontab replacement');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute( InputInterface $input, OutputInterface $output )
    {
        $crontabFile = getcwd().'/web/cronbike';

        $process = new Process('crontab '.$crontabFile);
        $process->run();
        if($process->isSuccessful()){
            $output->writeLn('<fg=green>Updated</fg=green>');
        } else {
            $output->writeLn('<fg=red> Error: '.$process->getErrorOutput().'</fg=red>');
        }
    }
} 