<?php

namespace Core\CronBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Core\CronBundle\Manager\Cron;


class CronManager {

    protected $container;

    function __construct($container){
        $this->container = $container;
    }

    function putJob(Cron $cron){
        $command = '';

        if(!is_null($cron->getComment())){
            $command .= PHP_EOL.'#'.$cron->getComment().PHP_EOL;
        }

        $command .= $cron->getExpression();
        $command .= ' ';
        $command .= $cron->getCommand();

        if(!is_null($cron->getLogFile())){
            $command .= ' >> ';
            $command .= $cron->getLogFile();
            $command .= ' 2>&1';
        }

        file_put_contents('./cronbike', $command.PHP_EOL, FILE_APPEND);
    }

    function putRaw($content){
        $content = str_replace(array("\r\n", "\r", "\n"), "\n", $content);
        file_put_contents('./cronbike', $content);
    }

    function getJobs(){
        return file_get_contents(getcwd().'/cronbike');
    }
} 