<?php

namespace Core\ProjectBundle\Query;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

class DraftFilter extends SQLFilter{

    /**
     * Exclude draft materials form query
     *
     * @return string The constraint SQL if there is available, empty string otherwise
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {

        if(!($targetEntity->reflClass->implementsInterface('Core\ProjectBundle\Query\DraftAware'))){
            return "";
        }

        return $targetTableAlias . '.is_draft IS NULL OR ' . $targetTableAlias . '.is_draft = 0';
    }
}