<?php

namespace Core\ProjectBundle\Command;


use Core\NodeBundle\Entity\NodeRepository;
use Core\ProjectBundle\Entity\Project;
use Core\SpiderBundle\Command\LockedCommand;
use Doctrine\Common\Cache\Cache;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProjectCountersCacheCommand extends LockedCommand {

    protected function configure()
    {
        $this
            ->setName('project:cache')
            ->setDescription('Обновляет кеш для счетчиков в проектах')
        ;
    }

    protected function exec(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var Cache $cache */
        $cache = $this->getContainer()->get('cache');
        /** @var NodeRepository $nodeRepo */
        $nodeRepo = $em->getRepository('CoreNodeBundle:Node');

        $projects = $em->getRepository('CoreProjectBundle:Project')->findAll();
        foreach($projects as $project){
            $counters = $nodeRepo->calcProjectCounters($project->getId());
            $cache->save(NodeRepository::PROJECT_CACHE_PREFIX.$project->getId(), $counters);
            $output->writeln('Счетчики для проекта: <fg=green>'.$project->getId().'</fg=green> обновлены');
        }
    }
}