<?php

namespace Core\SpiderBundle\Command;

use Core\NotificationBundle\Helpers\NotificationHelper;
use Core\SpiderBundle\CURL\ProxyChecker;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Core\SpiderBundle\Entity\ProxyServer;


class ProxyCheckCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('proxychecker')
            ->setDescription('Validate proxy server');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $notificationHelper = $this->getContainer()->get('notification');
        $checker = new ProxyChecker();
        $proxyRepo = $em->getRepository('CoreSpiderBundle:ProxyServer');
        $currTime = new \DateTime();

        $this->updateActual($em, $proxyRepo);

        $proxies = $proxyRepo->findByActual(true);

        foreach($proxies as $proxyServer){
            /**
             * @var ProxyServer $proxyServer
             */
            $output->write('Checking: <fg=green>' . (string)$proxyServer . '</fg=green>');

            $available = $checker->checkProxyServer($proxyServer);

            $proxyServer->setTransferTime((int)$available);

            if($available){
                $output->write(' - time <fg=green>' . $available . ' мс</fg=green>');
                $proxyServer->setLastAvailable($currTime);
                $proxyServer->setActual(true);
            } else {
                if(is_null($proxyServer->getLastAvailable())){
                    $proxyServer->setLastAvailable($currTime);
                }

                if(($currTime->getTimestamp() - $proxyServer->getLastAvailable()->getTimestamp())/3600 > 1){
                    $notificationHelper->add(NotificationHelper::PROXY_ERROR, $proxyServer->getId());
                    $proxyServer->setActual(false);
                }

                $output->write(' - <fg=red>invalid</fg=red>');
            }
            $em->flush($proxyServer);
            $output->writeln('');
        }
    }

    /**
     * @param $em
     * @param $proxyRepo
     */
    private function updateActual($em, $proxyRepo){
        $currTime = new \DateTime();
        $entities = $proxyRepo->getDailyNotActual();
        foreach($entities as $entity){
            /**
             * @var ProxyServer $entity
             */
            $entity->setActual(true);
            $entity->setLastUse($currTime);
        }
        $em->flush();
    }
} 