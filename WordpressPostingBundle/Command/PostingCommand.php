<?php

namespace Core\WordpressPostingBundle\Command;


use Core\NodeBundle\Entity\Node;
use Core\NodeBundle\Entity\NodeRepository;
use Core\SphinxBundle\QueryBuilder\QueryBuilder;
use Core\SpiderBundle\Command\LockedCommand;
use Core\WordpressPostingBundle\Entity\Blog;
use Core\WordpressPostingBundle\Entity\BlogRepository;
use Core\WordpressPostingBundle\Wordpress\Client;
use Doctrine\ORM\EntityManager;
use Seven\RpcBundle\Exception\InvalidXmlRpcContent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Exception\ContextErrorException;

class PostingCommand extends LockedCommand {

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('wordpress:posting')
            ->setDescription('Добавления материалов в Worpdress-блоги')
        ;
    }

    protected function exec(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var NodeRepository $nodeRepo */
        $nodeRepo = $em->getRepository('CoreNodeBundle:Node');
        /** @var BlogRepository $blogRepo */
        $blogRepo = $em->getRepository('CoreWordpressPostingBundle:Blog');

        $purifierConfig = \HTMLPurifier_Config::createDefault();
        $purifierConfig->set('AutoFormat.AutoParagraph',true);
        $purifierConfig->set('AutoFormat.RemoveEmpty',true);
        $purifierConfig->set('AutoFormat.RemoveEmpty.RemoveNbsp', true);
        $purifier = new \HTMLPurifier($purifierConfig);

        $startDate = new \DateTime();
        $startDate->modify('-1 day');
        $startDate->setTime(0, 0, 0);
        $endDate = new \DateTime();

        $sphinx = new QueryBuilder($em, 'CoreNodeBundle:Node', $this->getContainer());
        $sphinx->addBetweenDate('original', $startDate, $endDate);

        $blogs = $em->getRepository('CoreWordpressPostingBundle:Blog')->findAll();
        foreach($blogs as $blog){
            /** @var Blog $blog */

            try{
                $wordpress = new Client($blog->getUrl(), $blog->getUsername(), $blog->getPassword());
            } catch(ContextErrorException $e){
                $output->writeln($e->getMessage());
                $output->writeln('create exception: '.$blog->getId());
                continue;
            }


            $output->writeln($blog->getUrl());
            $output->writeln($blog->getQuery());

            $sphinx->setSearchParam($blog->getQuery(), $this->getContainer()->getParameter('highco_sphinx.main_index'));
            $sphinx->setLimits(0, 1000, 1000);
            $result =  $sphinx->getSphinxResult();
            if(isset($result['matches'])){
                $nodeIds = array_keys($result['matches']);

                $nodeIds = array_diff($nodeIds, $blogRepo->getPublishedNodeIds($blog));

                if(count($nodeIds) > 0){
                    $nodes = $nodeRepo->findByIds($nodeIds);
                    foreach($nodes as $node){
                        /** @var Node $node */
                        try{
                            $postId = $wordpress->newPost($node->getTitle(), $purifier->purify($node->getBody()), str_replace('"', '', $blog->getQuery()), $blog->getCategory());
                        }catch(ContextErrorException $e){
                            $output->writeln($e->getMessage());
                            $output->writeln('node problem: '.$node->getId());
                            continue;
                        }catch(InvalidXmlRpcContent $e){
                            $output->writeln($e->getMessage());
                            $output->writeln('node problem: '.$node->getId());
                            continue;
                        }
                        if($postId){
                            $output->writeln('<fg=green>Post added: '.$node->getTitle().'</fg=green>');
                            $blog->addNode($node);
                        }
                    }
                    $em->flush($blog);
                } else {
                    $output->writeln('<fg=yellow>Empty documents</fg=yellow>');
                }
            } else {
                $output->writeln('<fg=yellow>Empty documents</fg=yellow>');
            }
        }
    }
}