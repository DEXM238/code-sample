<?php

namespace Core\WordpressPostingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Blog
 *
 * @ORM\Table(name="anp_wordpress_blog")
 * @ORM\Entity(repositoryClass="Core\WordpressPostingBundle\Entity\BlogRepository")
 */
class Blog {

    const SECRET_KEY = 'buQuvu4u';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="string", length=255)
     */
    private $query;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255)
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="Core\NodeBundle\Entity\Node", inversedBy="blog")
     * @ORM\JoinTable(name="anp_blog_node")
     */
    private $nodes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Blog
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Blog
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Blog
     */
    public function setPassword($password)
    {
        $encrypted = base64_encode($this->strcode($password, self::SECRET_KEY));
        $this->password = $encrypted;
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        $decrypted = $this->strcode(base64_decode($this->password), self::SECRET_KEY);
        return $decrypted;
    }

    /**
     * Set query
     *
     * @param string $query
     * @return Blog
     */
    public function setQuery($query)
    {
        $this->query = $query;
    
        return $this;
    }

    /**
     * Get query
     *
     * @return string 
     */
    public function getQuery()
    {
        return $this->query;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nodes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add nodes
     *
     * @param \Core\NodeBundle\Entity\Node $nodes
     * @return Blog
     */
    public function addNode(\Core\NodeBundle\Entity\Node $nodes)
    {
        $this->nodes[] = $nodes;
    
        return $this;
    }

    /**
     * Remove nodes
     *
     * @param \Core\NodeBundle\Entity\Node $nodes
     */
    public function removeNode(\Core\NodeBundle\Entity\Node $nodes)
    {
        $this->nodes->removeElement($nodes);
    }

    /**
     * Get nodes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    private function strcode($str, $passw="")
    {
        $salt = "treWrakaVEDr@B!aw&m4=ruq2CAse*";
        $len = strlen($str);
        $gamma = '';
        $n = $len>100 ? 8 : 2;
        while( strlen($gamma)<$len )
        {
            $gamma .= substr(pack('H*', sha1($passw.$gamma.$salt)), 0, $n);
        }
        return $str^$gamma;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Blog
     */
    public function setCategory($category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }
}