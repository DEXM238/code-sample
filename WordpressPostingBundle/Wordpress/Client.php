<?php

namespace Core\WordpressPostingBundle\Wordpress;

class Client {

    protected $xmlRpcClient;
    protected $username;
    protected $password;

    public function __construct($url, $username, $password){
        $url = rtrim($url, '/');
        $this->xmlRpcClient = new \Seven\RpcBundle\XmlRpc\Client(implode('/', [$url, 'xmlrpc.php']));
        $this->username = $username;
        $this->password = $password;
    }

    public function newPost($title, $body, $keywords, $category){

        $content = [
            'title' => htmlentities($title, ENT_NOQUOTES, 'UTF-8'),
            'description' => $body,
            'mt_keywords' => htmlentities($keywords, ENT_NOQUOTES, 'UTF-8'),
            'mt_allow_comments'=> 1,
            'categories'=>[$category],
            'mt_allow_pings'=> 0,
            'post_type'=>'post',
        ];

        $params = [0, $this->username, $this->password, $content, true];
        return $this->xmlRpcClient->call('metaWeblog.newPost', $params);
    }

} 