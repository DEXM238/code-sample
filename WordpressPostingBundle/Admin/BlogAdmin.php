<?php

namespace Core\WordpressPostingBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class BlogAdmin extends Admin {

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;
    protected $baseRouteName = 'blog_action';
    protected $baseRoutePattern = 'blog/action';

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(\Sonata\AdminBundle\Show\ShowMapper $showMapper)
    { }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('url', null, array('label' => 'URL'))
            ->add('username', null, array('label' => 'Логин'))
            ->add('password', null, array('label' => 'Пароль'))
            ->add('query', null, array('label' => 'Запрос'))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(\Sonata\AdminBundle\Datagrid\ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('url', null, array('label' => 'URL'))
            ->addIdentifier('username', null, array('label' => 'Логин'))
            ->addIdentifier('query', null, array('label' => 'Запрос'))
        ;
    }

    /**
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('url', null, array('label' => 'URL'))
            ->add('username', null, array('label' => 'Логин'))
            ->add('query', null, array('label' => 'Запрос'))
        ;
    }

    public function getTemplate($name)
    {
        return parent::getTemplate($name);

    }
} 